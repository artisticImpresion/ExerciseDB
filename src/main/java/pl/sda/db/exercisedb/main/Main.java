/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.db.exercisedb.main;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;



/**
 *
 * @author artisticImpresion
 */
public class Main {
    
    public static void main(String[] args){
        
       SessionFactory instance = ConfigHibernate.getInstance();
       Session session = instance.openSession();
       
       Transaction tx = null;
       try{
           tx = session.beginTransaction();
           tx.commit();
       } catch(HibernateException e){
           if(tx != null) tx.rollback();
           e.printStackTrace();
       }finally{
           session.close();
       }
       
        
        
    }
    
}
