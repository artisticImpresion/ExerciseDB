/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.db.exercisedb.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author artisticImpresion
 */
@Entity
@Table(name = "ships")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ships.findAll", query = "SELECT s FROM Ships s"),
    @NamedQuery(name = "Ships.findByShipsId", query = "SELECT s FROM Ships s WHERE s.shipsId = :shipsId"),
    @NamedQuery(name = "Ships.findByShipName", query = "SELECT s FROM Ships s WHERE s.shipName = :shipName")})
public class Ship implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ships_id")
    private Integer shipsId;
    @Column(name = "ship_name")
    private String shipName;
    @JoinColumn(name = "ships_id", referencedColumnName = "journeys_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Journey journeys;

    public Ship() {
    }

    public Ship(Integer shipsId) {
        this.shipsId = shipsId;
    }

    public Integer getShipsId() {
        return shipsId;
    }

    public void setShipsId(Integer shipsId) {
        this.shipsId = shipsId;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public Journey getJourneys() {
        return journeys;
    }

    public void setJourneys(Journey journeys) {
        this.journeys = journeys;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shipsId != null ? shipsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ship)) {
            return false;
        }
        Ship other = (Ship) object;
        if ((this.shipsId == null && other.shipsId != null) || (this.shipsId != null && !this.shipsId.equals(other.shipsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.sda.db.exercisedb.Ships[ shipsId=" + shipsId + " ]";
    }
    
}
