/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.db.exercisedb.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author artisticImpresion
 */
@Entity
@Table(name = "reservations")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reservations.findAll", query = "SELECT r FROM Reservations r"),
    @NamedQuery(name = "Reservations.findByReservationsId", query = "SELECT r FROM Reservations r WHERE r.reservationsId = :reservationsId"),
    @NamedQuery(name = "Reservations.findByReservationsJourneysId", query = "SELECT r FROM Reservations r WHERE r.reservationsJourneysId = :reservationsJourneysId"),
    @NamedQuery(name = "Reservations.findByReservationDates", query = "SELECT r FROM Reservations r WHERE r.reservationDates = :reservationDates"),
    @NamedQuery(name = "Reservations.findByPrices", query = "SELECT r FROM Reservations r WHERE r.prices = :prices"),
    @NamedQuery(name = "Reservations.findByResevationsShipsId", query = "SELECT r FROM Reservations r WHERE r.resevationsShipsId = :resevationsShipsId")})
public class Reservation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "reservation_dates")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reservationDates;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "prices")
    private BigDecimal prices;
    
    @ManyToMany
    @JoinTable(name="customers_reservation",
            joinColumns={
            @JoinColumn(name="reservation_id")}, inverseJoinColumns={
            @JoinColumn(name="customer_id")})
    private Collection<Customer> customers;

    public Reservation(Integer id, Date reservationDates, BigDecimal prices, Collection<Customer> customers) {
        this.id = id;
        this.reservationDates = reservationDates;
        this.prices = prices;
        this.customers = customers;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Collection<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(Collection<Customer> customers) {
        this.customers = customers;
    }

    public Date getReservationDates() {
        return reservationDates;
    }

    public void setReservationDates(Date reservationDates) {
        this.reservationDates = reservationDates;
    }

    public BigDecimal getPrices() {
        return prices;
    }

    public void setPrices(BigDecimal prices) {
        this.prices = prices;
    }

    
    
}
