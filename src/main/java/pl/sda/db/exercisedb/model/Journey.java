/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.db.exercisedb.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author artisticImpresion
 */
@Entity
@Table(name = "journeys")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Journeys.findAll", query = "SELECT j FROM Journeys j"),
    @NamedQuery(name = "Journeys.findByJourneysId", query = "SELECT j FROM Journeys j WHERE j.journeysId = :journeysId"),
    @NamedQuery(name = "Journeys.findByDestinations", query = "SELECT j FROM Journeys j WHERE j.destinations = :destinations"),
    @NamedQuery(name = "Journeys.findByJourneysDates", query = "SELECT j FROM Journeys j WHERE j.journeysDates = :journeysDates"),
    @NamedQuery(name = "Journeys.findByJourneysShipsId", query = "SELECT j FROM Journeys j WHERE j.journeysShipsId = :journeysShipsId")})
public class Journey implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "journeys_id")
    private Integer journeysId;
    @Column(name = "destinations")
    private String destinations;
    @Column(name = "journeys_dates")
    @Temporal(TemporalType.TIMESTAMP)
    private Date journeysDates;
    @Column(name = "journeys_ships_id")
    private Integer journeysShipsId;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "journeys")
    private Ship ships;

    public Journey() {
    }

    public Journey(Integer journeysId) {
        this.journeysId = journeysId;
    }

    public Integer getJourneysId() {
        return journeysId;
    }

    public void setJourneysId(Integer journeysId) {
        this.journeysId = journeysId;
    }

    public String getDestinations() {
        return destinations;
    }

    public void setDestinations(String destinations) {
        this.destinations = destinations;
    }

    public Date getJourneysDates() {
        return journeysDates;
    }

    public void setJourneysDates(Date journeysDates) {
        this.journeysDates = journeysDates;
    }

    public Integer getJourneysShipsId() {
        return journeysShipsId;
    }

    public void setJourneysShipsId(Integer journeysShipsId) {
        this.journeysShipsId = journeysShipsId;
    }

    public Ship getShips() {
        return ships;
    }

    public void setShips(Ship ships) {
        this.ships = ships;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (journeysId != null ? journeysId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Journey)) {
            return false;
        }
        Journey other = (Journey) object;
        if ((this.journeysId == null && other.journeysId != null) || (this.journeysId != null && !this.journeysId.equals(other.journeysId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pl.sda.db.exercisedb.Journeys[ journeysId=" + journeysId + " ]";
    }
    
}
